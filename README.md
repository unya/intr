# intr - allow a command to be interruptible

## NAME
    intr - allow a command to be interruptible

    intr command has SunOS, long ago.

## SYNOPSIS
     intr [ -anvs ] [ -t seconds ] command [ arguments ]

## OPTIONS
     -v        Echo the command in  the  form  '  command'  (note leading SPACE).

     -a        Echo the command and its arguments.

     -n        Do not echo a NEWLINE after the command  or  argu-
               ments (for example `echo -n ...').

     -t secs   Arrange to have a SIGALRM signal delivered to  the
               command in secs seconds.

     -s        Silently in SIGALRM message.

## EXAMPLE
    $ intr -t 10 sleep 100

    FreeBSD(8.x)
    Centos(6.x)
    MAC OSX(10.8.4)

[Original code](http://www.yk.rim.or.jp/~unya/intr/intr.c)
